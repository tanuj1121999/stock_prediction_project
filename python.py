import numpy as np
import matplotlib
import sklearn
import pandas as pd
import os
import time
from datetime import datetime

import matplotlib.pyplot as plt
from matplotlib import style
style.use("ggplot")

from sklearn import datasets
from sklearn import svm,preprocessing

import re

import urllib.request

import sys
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import QUrl
from PyQt5.QtWebEngineWidgets import QWebEnginePage
import bs4 as bs

path="/Users/tanujanuj/Downloads/intraQuarter"

class Page(QWebEnginePage):
    def __init__(self, url):
        self.app = QApplication(sys.argv)
        QWebEnginePage.__init__(self)
        self.html = ''
        self.loadFinished.connect(self._on_load_finished)
        self.load(QUrl(url))
        self.app.exec_()

    def _on_load_finished(self):
        self.html = self.toHtml(self.Callable)
        print('Load finished')

    def Callable(self, html_str):
        self.html = html_str
        self.app.quit()


def main():
    statspath=path+"/_KeyStats"
    stock_list=[x[0] for x in os.walk(statspath)]
    for e in stock_list[2:10]:
        try:
            e=e.replace("/Users/tanujanuj/Downloads/intraQuarter/_KeyStats/","")
            page = Page('https://in.finance.yahoo.com/quote/'+e.upper()+'/key-statistics?p='+e.upper())
            soup = bs.BeautifulSoup(page.html, 'html.parser')
            save="forward/"+str(e)+".html"
            store= open(save,"w")
            store.write(str(soup))
            store.close()
        except Exception as e:
            print(e)
            time.sleep(2)

main()